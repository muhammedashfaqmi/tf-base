output "kiali_username" {
  value = local.ansible_kiali.username
}

output "kiali_password" {
  value = local.ansible_kiali.password
}
