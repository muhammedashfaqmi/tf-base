locals {
  vpc_id = data.aws_vpc.main.id

  vpc_public_subnets        = data.aws_subnet.subnet_public.*.id
  vpc_public_subnets_cidrs  = data.aws_subnet.subnet_public.*.cidr_block
  vpc_private_subnets       = data.aws_subnet.subnet_private.*.id
  vpc_private_subnets_cidrs = data.aws_subnet.subnet_private.*.cidr_block

  vpc_subnets       = local.use_public_network ? local.vpc_public_subnets : local.vpc_private_subnets
  vpc_subnets_cidrs = local.use_public_network ? concat(
  local.vpc_public_subnets_cidrs,
  local.vpc_private_subnets_cidrs
  ) : local.vpc_private_subnets_cidrs

  vpc_zones = data.aws_subnet.subnet_private.*.availability_zone
}


locals {
  permission_boundary = data.aws_iam_policy.permission-boundary.arn
}

locals {
  tags_common = merge(local.tags_common_base, {
    format(local.shared_tag, "environment") = var.environment

    format(local.shared_tag, "extras/terraform-module/${var.module_name}/version") = "${var.module_version}-${var.environment}"
  })

  tags = merge(local.tags_common, {
    Department = "busw"

    format(local.shared_tag, "terraform-module") = var.module_name
  })
}

locals {
  gitlab_runners_configs = {
    default = {
      linux   = {
        type  = "t3.large"
        count = 2
      }
      windows = {
        type  = "t3.large"
        count = 0
      }
    }
    base    = {
      linux   = {
        type  = "t3.xlarge"
        count = 3
      }
      windows = {
        type  = "t2.large"
        count = 2
      }
    }
  }

  environment_type     = local.env.is_basic ? "base" : "default"
  gitlab_runner_config = lookup(local.gitlab_runners_configs, local.environment_type, local.gitlab_runners_configs.default)
  prometheus_config    = lookup(local.gitlab_runners_configs, local.environment_type, local.gitlab_runners_configs.default)
}

locals {
  oag_hosts          = {
    china-stage = ""
    arch        = ""
    dev         = "hc-apigw-d.hilti.com"
    qa          = "hc-apigw-q.hilti.com"
    test        = "hc-apigw-q.hilti.com"
    stage       = "hc-apigw-q.hilti.com"
    prod        = "cloudapis.hilti.com"
  }
  oag_endpoints      = {
    china-stage = ""
    arch        = ""
    dev         = "https://${local.oag_hosts["dev"]}"
    qa          = "https://${local.oag_hosts["qa"]}"
    test        = "https://${local.oag_hosts["test"]}"
    stage       = "https://${local.oag_hosts["stage"]}"
    prod        = "https://${local.oag_hosts["prod"]}"
  }
  oag_full_endpoints = {
    china-stage = ""
    arch        = ""
    dev         = "${local.oag_endpoints["dev"]}/asws/dev/%s/%s/%s"
    qa          = "${local.oag_endpoints["qa"]}/asws/qa/%s/%s/%s"
    test        = "${local.oag_endpoints["test"]}/asws/test/%s/%s/%s"
    stage       = "${local.oag_endpoints["stage"]}/asws/stage/%s/%s/%s"
    prod        = "${local.oag_endpoints["prod"]}/asws/%s/%s/%s"
  }
  oag_host           = local.oag_hosts[var.environment]
  oag_endpoint       = local.oag_endpoints[var.environment]
  oag_endpoint_full  = local.oag_full_endpoints[var.environment]

  oag_available = local.oag_endpoint != "" && local.oag_endpoint_full != ""
}

locals {
  dns_main_zone       = local.is_legacy_env ? local.dns_arch_main_zone : element(concat(data.aws_route53_zone.hilti-cloud.*.zone_id, [
    ""]), 0)
  dns_services_zone   = local.is_legacy_env ? local.dns_arch_services_zone : local.dns_main_zone
  dns_main_suffix     = local.is_legacy_env ? local.dns_arch_main_suffix : format("%s.%s", var.environment, var.dns_managed_name)
  dns_services_suffix = local.is_legacy_env ? local.dns_arch_services_suffix : local.dns_main_suffix

  certificate_services = local.is_legacy_env ? element(concat(aws_iam_server_certificate.legacy-cert-services.*.arn, [""]), 0) : element(concat(data.aws_acm_certificate.hilti-cloud.*.arn, [""]), 0)
}

locals {
  is_basic_env       = local.env.is_basic
  is_legacy_env      = var.environment == "arch"
  use_public_network = length(local.env.vpc_subnets_public) > 0
  managed_env        = var.env_managed_map[var.environment]
}
