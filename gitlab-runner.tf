module "gitlab-runner" {
  source = "./gitlab-runner"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  ami = data.aws_ami.aws_linux.image_id

  aws_region      = var.aws_region
  aws_zones       = [
    local.vpc_zones[0],
    local.vpc_zones[1],
    local.vpc_zones[2],
  ]
  aws_vpc_subnets = [
    local.vpc_subnets[0],
    local.vpc_subnets[1],
    local.vpc_subnets[2],
  ]
  aws_vpc_id      = local.vpc_id

  gitlab_domain = "gitlab.hilti.com"
  runner_count  = local.gitlab_runner_config.linux.count
  module_prefix = var.module_prefix

  key_name            = aws_key_pair.user.key_name
  infra_bucket        = aws_s3_bucket.infra.bucket
  infra_prefix        = var.infra_prefix
  runner_cache_bucket = aws_s3_bucket.infra.bucket
  iam_boundary        = local.permission_boundary
  environment         = var.environment

  tags = merge(local.tags, {
    format(local.shared_tag, "role") = "Gitlab"
  })

  runner_tags               = [
    "aws",
    "type-docker",
    format("%s-%s", "environment", lower(var.environment)),
  ]
  runner_instance_type      = local.gitlab_runner_config.linux.type
  runner_registration_token = "a5xR7FNR3Whs9frpnj3k"
  runner_version            = "v12.9.1"
  runner_limit              = "20"
  runner_root_disk_size     = "20"
  runner_disk_size          = "500"
  runner_disk_inodes        = "67108864"
  runner_cache_prefix       = "gitlab/cache/"
  runner_type               = "docker"

  dns_create = true
  dns_zone   = local.dns_main_zone
  dns_suffix = local.dns_main_suffix
}

module "gitlab-runner-windows" {
  source = "./gitlab-runner"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

 # ami = data.aws_ami.aws_windows_2012.image_id
 ami = "ami-08493d85988735216"
 
  
  aws_region      = var.aws_region
  aws_zones       = [
    local.vpc_zones[0],
    local.vpc_zones[1],
    local.vpc_zones[2],
  ]
  aws_vpc_subnets = [
    local.vpc_subnets[0],
    local.vpc_subnets[1],
    local.vpc_subnets[2],
  ]
  aws_vpc_id      = local.vpc_id

  gitlab_domain = "gitlab.hilti.com"
  runner_count  = local.gitlab_runner_config.windows.count
  module_prefix = var.module_prefix

  key_name            = aws_key_pair.user.key_name
  infra_bucket        = aws_s3_bucket.infra.bucket
  infra_prefix        = var.infra_prefix
  runner_cache_bucket = aws_s3_bucket.infra.bucket
  iam_boundary        = local.permission_boundary
  environment         = var.environment

  tags = merge(local.tags, {
    format(local.shared_tag, "role") = "Gitlab"
  })

  runner_tags               = [
    "aws",
    "type-windows",
    format("%s-%s", "environment", lower(var.environment)),
  ]
  runner_instance_type      = local.gitlab_runner_config.windows.type
  runner_registration_token = "a5xR7FNR3Whs9frpnj3k"
  runner_version            = "v12.4.1"
  runner_limit              = "20"
  runner_root_disk_size     = "150"
  runner_disk_size          = "500"
  runner_cache_prefix       = "gitlab/cache/"
  runner_type               = "windows"

  dns_create = true
  dns_zone   = local.dns_main_zone
  dns_suffix = local.dns_main_suffix
}
