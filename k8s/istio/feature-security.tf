locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#SecurityFeatureSpec
  feature_security = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CitadelComponentSpec
      citadel = {
        enabled = true

        k8s = merge(local.k8s_default_config, {})
      }

      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CertManagerComponentSpec
      certManager = {
        enabled = false
      }

      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#NodeAgentComponentSpec
      nodeAgent = {
        enabled = false
      }
    }
  }
}
