locals {
  name       = var.name
  local_tags = {
    Name = local.name
  }
  tags       = merge(var.tags, local.local_tags)
}

locals {
  data_dir = "${path.module}/data"

  ansible_directory = "${path.root}/artifacts/ansible/infra/20-${local.name}"

  ansible_tags = []

  system_srv_tolerations   = [
    {
      key      = replace(format(var.shared_tag, "k8s-node-role"), "https://", "")
      operator = "Equal"
      value    = "system"
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-service"), "https://", "")
      operator = "Equal"
      value    = "k8s"
      effect   = "NoSchedule"
    },
    {
      key      = replace(format(var.shared_tag, "k8s-node-name"), "https://", "")
      operator = "Equal"
      value    = "k8s-istio"
      effect   = "NoSchedule"
    },
  ]
  system_srv_node_selector = map(
  replace(format(var.shared_tag, "k8s-node-role"), "https://", ""),
  "system",
  replace(format(var.shared_tag, "k8s-node-service"), "https://", ""),
  "k8s",
  replace(format(var.shared_tag, "k8s-node-name"), "https://", ""),
  "k8s-istio",
  )
}
