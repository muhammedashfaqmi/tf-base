locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#TrafficManagementFeatureSpec
  feature_trafficmanagement = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#PilotComponentSpec
      pilot = {
        enabled = true

        k8s = merge(local.k8s_default_config, {
          env = [
            {
              name      = "POD_NAME"
              valueFrom = {
                fieldRef = {
                  apiVersion = "v1"
                  fieldPath  = "metadata.name"
                }
              }
            },
            {
              name      = "POD_NAMESPACE"
              valueFrom = {
                fieldRef = {
                  apiVersion = "v1"
                  fieldPath  = "metadata.namespace"
                }
              }
            },
            {
              name  = "GODEBUG"
              value = "gctrace=1"
            },
            {
              name  = "PILOT_TRACE_SAMPLING"
              value = "100"
            },
            {
              name  = "CONFIG_NAMESPACE"
              value = "istio-config"
            }
          ]

          hpaSpec = {
            minReplicas    = 2
            maxReplicas    = 5
            metrics        = [
              {
                resource = {
                  name                     = "cpu"
                  targetAverageUtilization = 80
                }
                type     = "Resource"
              }
            ]
            scaleTargetRef = {
              apiVersion = "apps/v1"
              kind       = "Deployment"
              name       = "istio-pilot"
            }
          }

          resources = {
            requests = {
              cpu    = "10m"
              memory = "100Mi"
            }
            limits   = {
              cpu    = "1000m"
              memory = "1G"
            }
          }
        })
      }

      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#ProxyComponentSpec
      proxy = {
        enabled = true

        k8s = merge(local.k8s_default_config, {})
      }
    }
  }
}
