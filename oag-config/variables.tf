variable "infra_bucket" {
  type = string
}

variable "infra_prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "create" {
  type    = bool
  default = false
}

variable "tags" {
  type    = map(string)
  default = {}
}

