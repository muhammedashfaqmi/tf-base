locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#GatewayFeatureSpec
  feature_gateways = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#IngressGatewayComponentSpec
      ingressGateway = {
        enabled = true

        k8s = merge(local.k8s_default_config, {
          hpaSpec = {
            minReplicas    = 2
            maxReplicas    = 5
            metrics        = [
              {
                resource = {
                  name                     = "cpu"
                  targetAverageUtilization = 80
                }
                type     = "Resource"
              }
            ]
            scaleTargetRef = {
              apiVersion = "apps/v1"
              kind       = "Deployment"
              name       = "istio-ingressgateway"
            }
          }

          resources = {
            requests = {
              cpu    = "10m"
              memory = "40Mi"
            }
            limits   = {
              cpu    = "2000m"
              memory = "1024Mi"
            }
          }
        })
      }

      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#EgressGatewayComponentSpec
      egressGateway = {
        enabled = true

        k8s = merge(local.k8s_default_config, {
          hpaSpec = {
            minReplicas    = 2
            maxReplicas    = 5
            metrics        = [
              {
                resource = {
                  name                     = "cpu"
                  targetAverageUtilization = 80
                }
                type     = "Resource"
              }
            ]
            scaleTargetRef = {
              apiVersion = "apps/v1"
              kind       = "Deployment"
              name       = "istio-egressgateway"
            }
          }

          resources = {
            requests = {
              cpu    = "10m"
              memory = "40Mi"
            }
            limits   = {
              cpu    = "2000m"
              memory = "1024Mi"
            }
          }
        })
      }
    }
  }

  # https://istio.io/docs/reference/config/installation-options/#gateways-options
  values_gateways = {
    enabled = true

    istio-ingressgateway = {
      # https://istio.io/docs/tasks/traffic-management/ingress/ingress-certmgr/
      sds = {
        enabled   = true
        image     = "node-agent-k8s"
        resources = local.k8s_default_config.resources
      }
    }
  }
}
