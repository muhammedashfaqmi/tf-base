resource "aws_eks_cluster" "this" {
  count = local.create_resources
  name  = local.name

  role_arn = data.aws_iam_role.eks_role[0].arn

  version = var.k8s_version

  vpc_config {
    endpoint_public_access  = true
    //    endpoint_public_access  = var.use_public_network
    endpoint_private_access = true
    subnet_ids              = var.vpc_subnets
    security_group_ids      = [aws_security_group.k8s-master[0].id]
  }

  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  depends_on = [
    aws_security_group_rule.k8s-master-nodes-linux-ingress-443,
  ]

  timeouts {
    create = "30m"
    update = "30m"
  }
}

resource "aws_security_group" "k8s-master" {
  count       = local.create_resources
  name        = "${local.name}-master"
  description = "Security group for EKS"
  vpc_id      = var.vpc_id

  tags = local.tags
}

resource "aws_security_group_rule" "k8s-master-egress" {
  count             = local.create_resources
  description       = "Allow master to talk to anything"
  type              = "egress"
  security_group_id = aws_security_group.k8s-master[0].id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "k8s-master-nodes-linux-ingress-443" {
  count                    = var.create ? var.api_sg_count + 1 : 0
  description              = "Allow other SGs to communicate with the cluster API Server"
  type                     = "ingress"
  security_group_id        = aws_security_group.k8s-master[0].id
  source_security_group_id = local.api_sources_sgs_ids[count.index]
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "k8s-master-nodes-linux-ingress-443-all" {
  count             = var.create && var.use_public_network ? 1 : 0
  description       = "Allow everyone to communicate with the cluster API Server"
  type              = "ingress"
  security_group_id = aws_security_group.k8s-master[0].id
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
}
