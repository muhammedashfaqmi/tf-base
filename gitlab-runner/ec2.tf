resource "aws_security_group" "gitlab-runner" {
  count       = local.create-instances
  name        = local.name
  description = "Security group for Gitlab runner instance"
  vpc_id      = var.aws_vpc_id

  tags = local.tags
}

resource "aws_security_group_rule" "gitlab-runner-egress" {
  count             = local.create-instances
  type              = "egress"
  security_group_id = aws_security_group.gitlab-runner[0].id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "gitlab-runner-ingress-ssh" {
  count             = var.runner_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.gitlab-runner[0].id
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "gitlab-runner-ingress-rdp" {
  count             = var.runner_type == "windows" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.gitlab-runner[0].id
  from_port         = 3389
  to_port           = 3389
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "gitlab-runner-ingress-winrm-secure" {
  count             = var.runner_type == "windows" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.gitlab-runner[0].id
  from_port         = 5986
  to_port           = 5986
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "gitlab-runner" {
  count             = var.runner_count
  ami               = var.ami
  instance_type     = var.runner_instance_type
  availability_zone = var.aws_zones[count.index]

  vpc_security_group_ids = [aws_security_group.gitlab-runner[0].id]
  iam_instance_profile   = aws_iam_instance_profile.gitlab-runner[0].id

  key_name = var.key_name

  subnet_id = var.aws_vpc_subnets[count.index]

  user_data = data.null_data_source.gitlab-runner-data[count.index].outputs[format("init-%s", var.runner_type)]

  root_block_device {
    volume_type = "gp2"
    volume_size = var.runner_root_disk_size
  }

  ebs_block_device {
    device_name           = "/dev/sdh"
    volume_type           = "gp2"
    volume_size           = var.runner_disk_size
    encrypted             = var.runner_type == "docker"
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [user_data]
  }

  depends_on = [
    aws_cloudwatch_log_group.gitlab-runner,
    aws_iam_role_policy.gitlab-runner,
  ]

  tags = merge(
    local.tags,
    {
      "Name" = format("%s-%d", local.local-tags["Name"], count.index + 1)
    }
  )
  volume_tags = merge(
    local.tags,
    {
      "Name" = format("%s-%d", local.local-tags["Name"], count.index + 1)
    }
  )
}

locals {
  create-instances = var.runner_count > 0 ? 1 : 0
}

