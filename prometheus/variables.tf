variable "ami" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "aws_zones" {
  type = list(string)
}

variable "aws_vpc_id" {
  type = string
}

variable "aws_vpc_subnets" {
  type = list(string)
}

variable "infra_bucket" {
  type = string
}

variable "use_public_network" {
  type    = bool
  default = false
}

variable "infra_prefix" {
  type = string
}

variable "grafana_version" {
  type = string
}

variable "prometheus_count" {
  type = string
}

variable "prometheus_type" {
  type = string
}

variable "prometheus_instance_type" {
  type = string
}

variable "prometheus_version" {
  type = string
}

variable "prometheus_limit" {
  type = string
}

variable "prometheus_root_disk_size" {
  type = string
}

variable "prometheus_disk_size" {
  type = string
}

variable "prometheus_disk_inodes" {
  type    = string
  default = ""
}

variable "prometheus_cache_prefix" {
  type = string
}

variable "dns_create" {
  type    = bool
  default = false
}

variable "dns_zone" {
  default = ""
}

variable "dns_suffix" {
  default = ""
}

variable "prometheus_tags" {
  type    = list(string)
  default = []
}

variable "prometheus_cache_bucket" {
  type = string
}

variable "key_name" {
  type = string
}

variable "iam_boundary" {
  type = string
}

variable "environment" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "module_prefix" {
  default = ""
}

