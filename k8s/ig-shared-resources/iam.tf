resource "aws_iam_role" "ec2-nodes" {
  count = var.create ? 1 : 0

  name = "${local.name}-ec2-nodes"
  path = "/"

  permissions_boundary = var.iam_boundary
  assume_role_policy   = data.aws_iam_policy_document.ec2-assume[0].json

  tags = local.tags
}

data "aws_iam_policy_document" "ec2-assume" {
  count = var.create ? 1 : 0

  version = "2012-10-17"

  statement {
    effect  = "Allow"
    principals {
      identifiers = ["ec2.${data.aws_partition.current.dns_suffix}"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy" "ec2-nodes-AmazonEKSWorkerNodePolicy" {
  count = local.create_resources
  arn   = "arn:${data.aws_partition.current.partition}:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

resource "aws_iam_role_policy_attachment" "ec2-nodes-AmazonEKSWorkerNodePolicy" {
  count      = local.create_resources
  role       = aws_iam_role.ec2-nodes[0].name
  policy_arn = data.aws_iam_policy.ec2-nodes-AmazonEKSWorkerNodePolicy[0].arn
}

data "aws_iam_policy" "ec2-nodes-AmazonEKS_CNI_Policy" {
  count = local.create_resources
  arn   = "arn:${data.aws_partition.current.partition}:iam::aws:policy/AmazonEKS_CNI_Policy"
}

resource "aws_iam_role_policy_attachment" "ec2-nodes-AmazonEKS_CNI_Policy" {
  count      = local.create_resources
  role       = aws_iam_role.ec2-nodes[0].name
  policy_arn = data.aws_iam_policy.ec2-nodes-AmazonEKS_CNI_Policy[0].arn
}

data "aws_iam_policy" "ec2-nodes-AmazonEC2ContainerRegistryReadOnly" {
  count = local.create_resources
  arn   = "arn:${data.aws_partition.current.partition}:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "ec2-nodes-AmazonEC2ContainerRegistryReadOnly" {
  count      = local.create_resources
  role       = aws_iam_role.ec2-nodes[0].name
  policy_arn = data.aws_iam_policy.ec2-nodes-AmazonEC2ContainerRegistryReadOnly[0].arn
}

