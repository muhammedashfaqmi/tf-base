locals {
  addon_elastic_stack = {
    chart_version = "1.8.0"
    name          = "elastic-stack"
    namespace     = local.system_srv_namespace
    helm_values   = {
      elasticsearch = {
        enabled = false
      }

      kibana = {
        enabled = true

        image = {
          repository = split(":", local.urls_mappings[data.aws_partition.current.partition].kibana_oss)[0]
          tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].kibana_oss)[1]
        }

        tolerations  = local.system_srv_tolerations
        nodeSelector = local.system_srv_node_selector

        env = {
          ELASTICSEARCH_HOSTS = ""
        }

        ingress = {
          enabled = true

          annotations = {
            //"kubernetes.io/tls-acme" = "true"
            "certmanager.k8s.io/cluster-issuer" = "self-signed"
          }

          hosts = !var.create ? [] : [
            aws_route53_record.addon-kibana[0].name,
            aws_route53_record.addon-kibana-local[0].name,
          ]

          tls = [
            {
              secretName = "elastic-stack-kibana-tls"

              hosts = !var.create ? [] : [
                aws_route53_record.addon-kibana[0].name,
                aws_route53_record.addon-kibana-local[0].name,
              ]
            }]
        }

        resources = {
          requests = {
            cpu    = "100m"
            memory = "128Mi"
          }
          limits   = {
            cpu    = "500m"
            memory = "512Mi"
          }
        }
      }

      logstash = {
        enabled = false
      }
    }
  }
}

resource "aws_route53_record" "addon-kibana" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = "kibana.${var.dns_suffix}"
  type     = "CNAME"

  ttl = 60

  records = [
    aws_lb.ingress[0].dns_name,
  ]
}

resource "aws_route53_record" "addon-kibana-local" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = "local-kibana.${var.dns_suffix}"
  type     = "A"

  ttl = 60

  records = [
    "127.0.0.1"]
}
