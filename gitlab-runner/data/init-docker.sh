#!/usr/bin/env bash
set -xe

mkdir -p /mnt/data
mount /dev/sdh /mnt/data || mkfs.ext4 -N ${DISK_INODES} /dev/sdh
mount /dev/sdh /mnt/data || true
touch /mnt/data/mounted

yum update -y
yum install -y docker

if [[ ! -d /mnt/data/docker ]]; then
  mv /var/lib/docker /mnt/data/docker
fi
cat > /etc/docker/daemon.json <<EOF
{
  "storage-driver": "overlay2",
  "data-root": "/mnt/data/docker",
  "log-driver": "awslogs",
  "log-opts": {
    "tag": "runner-$${RUNNER_ID}.{{.Name}}.{{.ID}}",
    "awslogs-region": "${REGION}",
    "awslogs-group": "${LOG_GROUP}"
  }
}
EOF
service docker start
usermod -a -G docker ec2-user

docker run --detach \
    --name gitlab-runner \
    --restart always \
    --volume /mnt/data/gitlab-runner/config:/etc/gitlab-runner \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --privileged \
    gitlab/gitlab-runner:${RUNNER_VERSION}

docker exec -i gitlab-runner bash -c "apt-get update && apt-get -y install docker.io && chmod 777 /var/run/docker.sock"

if [[ $(docker exec -i gitlab-runner gitlab-runner list  |& grep 'Executor' |& wc -l) < 1 ]]; then
  docker exec -i gitlab-runner gitlab-runner register -n \
    --description "Docker-#$${RUNNER_ID} on AWS-${ENV}" \
    --url "https://${DOMAIN}" \
    --limit "0" \
    --request-concurrency "${RUNNER_LIMIT}" \
    --registration-token ${REGISTRATION_TOKEN} \
    --docker-privileged \
    --executor docker \
    --docker-image "docker:stable" \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --cache-shared \
    --cache-path "${CACHE_PREFIX}" \
    --cache-s3-bucket-name "${CACHE_BUCKET}" \
    --env "AWS_DEFAULT_REGION=${REGION}" \
    --env "ENVIRONMENT=${ENV_LOWER}" \
    --tag-list "${TAGS}"
fi

sed -i "s/^\(concurrent\s*=\s*\).*$/\1${RUNNER_LIMIT}/" /mnt/data/gitlab-runner/config/config.toml

# docker run -d \
#   --name cleanup \
#   -v /var/run/docker.sock:/var/run/docker.sock:rw \
#   -v /var/lib/docker:/var/lib/docker:rw \
#   --restart always \
#   meltwater/docker-cleanup:latest

echo '0 0 * * * docker system prune -a -f --volumes' > /var/spool/cron/root
