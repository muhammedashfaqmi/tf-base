resource "aws_cloudwatch_log_group" "gitlab-runner" {
  name              = "${local.name}/type-${var.runner_type}"
  retention_in_days = 7

  tags = local.tags
}

