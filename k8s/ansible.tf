module "ansible" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/ansible/0.1.0-master.zip"
  create = true

  project     = null
  service     = "infra"
  name        = local.name
  environment = var.environment
  tags        = local.tags

  use_remote_state = false

  module_prefix = var.module_prefix
  shared_tag    = var.shared_tag

  ansible_dir_source     = local.data_dir
  ansible_dir_target     = local.ansible_directory
  ansible_tags           = local.ansible_tags
  ansible_variables_json = local.ansible_variables_json
}

locals {
  ansible_variables_json = jsonencode({
    name_full  = local.name
    aws_region = var.aws_region

    k8s_cluster        = var.create ? aws_eks_cluster.this[0].name : null
    k8s_roles_mappings = local.k8s_roles_mappings

    helm               = local.addon_helm
    coredns            = local.addon_coredns
    # https://github.com/helm/charts/tree/master/stable/kubernetes-dashboard
    k8s_dashboard      = local.addon_k8s_dashboard
    # https://github.com/helm/charts/tree/master/stable/metrics-server
    metrics_server     = local.addon_metrics_server
    cert_manager       = local.addon_cert_manager
    ingress            = local.addon_ingress
    # https://github.com/helm/charts/tree/master/stable/kube2iam
    kube2iam           = local.addon_kube2iam
    cluster_autoscaler = local.addon_cluster_autoscaler
    elastic_stack      = local.addon_elastic_stack
    prometheus         = local.addon_prometheus
    grafana            = local.addon_grafana
    cloudwatch_exporter= local.addon_cloudwatch_exporter
  })
}
