module "k8s" {
  source = "./k8s"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  name         = "${var.module_prefix}${local.k8s_name}"
  create       = local.env.iam_eks_role != null
  iam_role     = local.env.iam_eks_role
  iam_boundary = local.permission_boundary

  key_name = aws_key_pair.user.key_name

  environment = var.environment
  tags        = merge(local.tags, {
    format(local.shared_tag, "role") = "K8s"
  })

  env = local.env

  ecr_mirror = aws_ecr_repository.mirror.repository_url

  aws_region = var.aws_region
  aws_zones  = local.vpc_zones

  vpc_id            = local.vpc_id
  vpc_subnets       = local.vpc_subnets
  vpc_subnets_cidrs = local.vpc_subnets_cidrs

  infra_bucket  = aws_s3_bucket.infra.bucket
  infra_prefix  = local.infra_prefix
  module_prefix = var.module_prefix
  shared_tag    = local.shared_tag

  dns_suffix = local.dns_main_suffix
  dns_zone   = local.dns_main_zone

  use_public_network = local.use_public_network

  lambda_stub_bucket = aws_s3_bucket_object.lambda-stub.bucket
  lambda_stub_key    = aws_s3_bucket_object.lambda-stub.key

  api_sources  = concat([
    {
      sg_id    = module.gitlab-runner.sg_id
      role     = module.gitlab-runner.iam_role_arn
      username = "aws:{{AccountID}}:instance:{{SessionName}}"
      groups   = [
        "system:masters",
      ]
    }], local.env.iam_eks_admin_role != null ? [
    {
      sg_id    = null
      role     = local.env.iam_eks_admin_role
      username = "admin:{{SessionName}}"
      groups   = [
        "system:masters",
      ]
    }] : [])
  api_sg_count = 1

  k8s_version = "1.14"
}

