data "aws_caller_identity" "current" {
}

data "aws_partition" "current" {}

data "aws_iam_role" "eks_role" {
  count = local.create_resources
  name  = var.iam_role
}

data "aws_ssm_parameter" "ami_linux" {
  name = "/aws/service/eks/optimized-ami/${var.k8s_version}/amazon-linux-2/recommended/image_id"
}

data "aws_ami" "eks_linux" {
  count = local.create_resources
  most_recent = true
  filter {
    name = "name"

    # https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html
    values = ["amazon-eks-node-${var.k8s_version}*"]
  }

  tags = {
    Project= "hilti-aws-managed-account"
  }
 
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["961992271922"]
}

#data "aws_ssm_parameter" "ami_windows_2019_full" {
#  name = "/aws/service/ami-windows-latest/Windows_Server-2019-English-Full-EKS_Optimized-${var.k8s_version}-2020.06.10/image_id"
#}

data "aws_acm_certificate" "hilti_cloud" {
 domain      = "*.${var.environment}.swu.hilti.cloud" 
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

data "aws_ami" "eks_windows" {
  count = local.create_resources
  most_recent = true

  filter {
    name = "name"
   
    values = ["Windows_Server-2019-English-Full-EKS_Optimized-${var.k8s_version}*"]
  }

   tags = {
    Project= "hilti-aws-managed-account"
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  
  owners = ["amazon"]
}
