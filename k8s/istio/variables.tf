variable "k8s_namespace" {
  type        = string
  description = "Istio namespace"
  default     = "istio-system"
}

variable "k8s_cluster" {
  type = string
}

variable "istio_version" {
  type        = string
  description = "Istio components version"
}

variable "istio_hub" {
  type        = string
  description = "Istio docker hub path"
  default     = "docker.io/istio"
}

variable "create" {
  type        = bool
  description = "If Istio is enabled"
}

variable "env" {
  type = object({
    account_id = string
  })
}

variable "environment" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "name" {
  type    = string
  default = "istio"
}

variable "module_prefix" {
  type = string
}

variable "shared_tag" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_subnets" {
  type = list(string)
}

variable "iam_boundary" {
  type = string
}

variable "infra_bucket" {
  type = string
}

variable "ec2_linux_ami" {
  type = string
}

variable "k8s_ec2_nodes_sg_id" {
  type = string
}

variable "k8s_iam_nodes_role_name" {
  type = string
}

variable "k8s_full_tolerations" {
  type = list(object({
    key      = string
    operator = string
    effect   = string
  }))
}
