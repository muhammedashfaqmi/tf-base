resource "aws_route53_record" "main" {
  count    = var.dns_create && var.use_public_network ? var.prometheus_count : 0
  provider = aws.route53
  zone_id  = var.dns_zone
  name = "monitoring.${var.dns_suffix}"
  type = "A"

  records = [element(aws_instance.prometheus.*.public_ip, count.index)]
  ttl     = "300"
}

resource "aws_route53_record" "internal" {
  count    = var.dns_create ? var.prometheus_count : 0
  provider = aws.route53
  zone_id  = var.dns_zone
  name = "monitoring-internal.${var.dns_suffix}"
  type = "A"

  records = [element(aws_instance.prometheus.*.private_ip, count.index)]
  ttl     = "300"
}
