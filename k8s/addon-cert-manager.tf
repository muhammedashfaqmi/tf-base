locals {
  addon_cert_manager = {
    chart_version = "v0.6.7"
    name          = "cert-manager"
    namespace     = local.system_srv_namespace

    webhook_deployment = jsonencode({
      spec = {
        template = {
          spec = {
            tolerations  = local.system_srv_tolerations
            nodeSelector = local.system_srv_node_selector
          }
        }
      }
    })
    webhook_cron       = jsonencode({
      spec = {
        jobTemplate = {
          spec = {
            template = {
              spec = {
                tolerations  = local.system_srv_tolerations
                nodeSelector = local.system_srv_node_selector
              }
            }
          }
        }
      }
    })

    helm_values = {
      image = {
        repository = split(":", local.urls_mappings[data.aws_partition.current.partition].cert_manager_controller)[0]
        tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].cert_manager_controller)[1]
      }

      replicaCount = 1

      annotations = {}

      tolerations  = local.system_srv_tolerations
      nodeSelector = local.system_srv_node_selector

      resources = {
        requests = {
          cpu    = "50m"
          memory = "128Mi"
        }
        limits   = {
          cpu    = "500m"
          memory = "512Mi"
        }
      }

      ingressShim = {
        defaultIssuerName = "letsencrypt-prod"
        defaultIssuerKind = "ClusterIssuer"
      }

      rbac = {
        create = true
      }

      webhook = {
        image = {
          repository = "quay.io/jetstack/cert-manager-webhook"
          tag        = "v0.6.2"
        }

        enabled = true

        replicaCount = 1

        annotations = {}

        resources = {
          requests = {
            cpu    = "100m"
            memory = "128Mi"
          }
          limits   = {
            cpu    = "500m"
            memory = "256Mi"
          }
        }
      }
    }
  }
}
