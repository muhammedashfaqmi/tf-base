locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CNIFeatureSpec
  feature_cni = {
    enabled = true

    # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CNIFeatureSpec-Components
    components = {
      # https://github.com/kubernetes/kubernetes/issues/60596
      namespace = "kube-system"

      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CNIComponentSpec
      cni = {
        enabled = true

        k8s = {
          tolerations = var.k8s_full_tolerations
        }
      }
    }
  }
}
