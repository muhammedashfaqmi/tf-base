locals {
  # https://github.com/helm/charts/tree/master/stable/kubernetes-dashboard
  addon_k8s_dashboard = {
    chart_version = "1.8.0"
    name          = "kubernetes-dashboard"
    namespace     = local.system_srv_namespace
    helm_values   = {
      image = {
        repository = split(":", local.urls_mappings[data.aws_partition.current.partition].kubernetes_dashboard)[0]
        tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].kubernetes_dashboard)[1]
      }

      replicaCount = 1

      annotations = {
        "cluster-autoscaler.kubernetes.io/safe-to-evict" = "true"
      }

      tolerations  = local.system_srv_tolerations
      nodeSelector = local.system_srv_node_selector

      resources = {
        requests = {
          cpu    = "100m"
          memory = "128Mi"
        }
        limits   = {
          cpu    = "500m"
          memory = "256Mi"
        }
      }

      ingress = {
        enabled = true

        annotations = {
          "nginx.ingress.kubernetes.io/backend-protocol" = "HTTPS"

          //"kubernetes.io/tls-acme" = "true"
          "certmanager.k8s.io/cluster-issuer" = "self-signed"
        }

        hosts = !var.create ? [] : [
          aws_route53_record.addon-k8s-dashboard[0].name,
          aws_route53_record.addon-k8s-dashboard-local[0].name,
        ]

        tls = [
          {
            secretName = "kubernetes-dashboard-tls"

            hosts = !var.create ? [] : [
              aws_route53_record.addon-k8s-dashboard[0].name,
              aws_route53_record.addon-k8s-dashboard-local[0].name,
            ]
          }]
      }

      rbac = {
        create = true

        clusterAdminRole = true

        serviceAccount = {
          create = true
        }
      }
    }
  }
}

resource "aws_route53_record" "addon-k8s-dashboard" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = "k8s-dashboard.${var.dns_suffix}"
  type     = "CNAME"

  ttl = 60

  records = [
    aws_lb.ingress[0].dns_name,
  ]
}

resource "aws_route53_record" "addon-k8s-dashboard-local" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = "local-k8s-dashboard.${var.dns_suffix}"
  type     = "A"

  ttl = 60

  records = [
    "127.0.0.1"]
}
