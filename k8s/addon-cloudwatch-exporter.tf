locals {
  addon_cloudwatch_exporter = {
    chart_version       = "0.8.0"
    name                = "prometheus-cloudwatch-exporter"
    namespace           = local.system_srv_namespace
    cloudwatch_iam_role = !var.create ? "" : aws_iam_role.cloudwatch-exporter[0].arn
    helm_values         = {
      image = {
        repository = split(":", local.urls_mappings[data.aws_partition.current.partition].prometheus_cloudwatch_exporter)[0]
        tag        = split(":", local.urls_mappings[data.aws_partition.current.partition].prometheus_cloudwatch_exporter)[1]
      }

      tolerations  = local.system_srv_tolerations
      nodeSelector = local.system_srv_node_selector

      config = var.environment == "dev" ? local.config_dev : local.config_main

      aws = {
        role = !var.create ? "" : aws_iam_role.cloudwatch-exporter[0].arn
      }

      ingress = {
        enabled = true

        annotations = {
          "nginx.ingress.kubernetes.io/backend-protocol" = "HTTP"
        }

        hosts = !var.create ? [] : [
          aws_route53_record.addon-cloudwatch-exporter-internal[0].name,
        ]
      }

      resources = {
        requests = {
          cpu    = "50m"
          memory = "128Mi"
        }
        limits   = {
          cpu    = "500m"
          memory = "512Mi"
        }
      }
    }
  }

  config_dev = <<CONFDEV
region: ${var.aws_region}
metrics:
 - aws_namespace: AWS/RDS
   aws_metric_name: DatabaseConnections
   aws_dimensions: [DbInstanceIdentifier]
   aws_dimension_select:
     DbInstanceIdentifier: [hilti-pe-services-user-settings]
 - aws_namespace: AWS/RDS
   aws_metric_name: DatabaseConnections
   aws_dimensions: [DbInstanceIdentifier]
   aws_dimension_select:
     DbInstanceIdentifier: [hilti-pe-services-product-information]
 - aws_namespace: AWS/RDS
   aws_metric_name: DatabaseConnections
   aws_dimensions: [DbInstanceIdentifier]
   aws_dimension_select:
     DbInstanceIdentifier: [hilti-pe-services-technical-database]
 - aws_namespace: AWS/RDS
   aws_metric_name: DatabaseConnections
   aws_dimensions: [DbInstanceIdentifier]
   aws_dimension_select:
     DbInstanceIdentifier: [hilti-pe-services-integration-services]
 - aws_namespace: AWS/RDS
   aws_metric_name: DatabaseConnections
   aws_dimensions: [DbInstanceIdentifier]
   aws_dimension_select:
     DbInstanceIdentifier: [hilti-pe-services-document-service-legacy]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-support-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-support-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-volume-calculator-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-volume-calculator-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-product-information]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-product-information]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-integration-services]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-integration-services]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-nps-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-nps-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-sns-reciever]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-sns-reciever]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-translations-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-translations-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-transformation-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-transformation-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-cad-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-cad-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-technical-database-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-technical-database-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-pe-gateway-sync-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-pe-gateway-sync-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-user-settings]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-user-settings]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-article-number-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-article-number-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-trimble-connect-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-trimble-connect-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-document-service-legacy]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-document-service-legacy]
   aws_statistics: [Average]
CONFDEV

  config_main = <<CONF
region: ${var.aws_region}
metrics:
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-support-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-support-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-volume-calculator-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-volume-calculator-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-product-information]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-product-information]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-integration-services]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-integration-services]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-nps-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-nps-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-sns-reciever]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-sns-reciever]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-translations-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-translations-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-transformation-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-transformation-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-cad-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-cad-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-technical-database-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-technical-database-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-pe-gateway-sync-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-pe-gateway-sync-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-user-settings]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-user-settings]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-article-number-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-article-number-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-trimble-connect-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-trimble-connect-service]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-document-service-legacy]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-pe-services-document-service-legacy]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-oauth-proxy]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-oauth-proxy]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-subscription]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-subscription]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-project]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-project]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-sharing]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-sharing]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-gateway]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-gateway]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-solution]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-solution]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-email]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-email]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-document]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-document]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-carina]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-carina]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-users]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-users]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-bom]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-bom]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-profservice]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-profservice]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-comment]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-comment]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-notification]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-notification]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-hierarchy]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-hierarchy]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-carina-client]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-carina-client]
   aws_statistics: [Average]

 - aws_namespace: AWS/ECS
   aws_metric_name: CPUUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-data-management]
   aws_statistics: [Average]
 - aws_namespace: AWS/ECS
   aws_metric_name: MemoryUtilization
   aws_dimensions: [ClusterName, ServiceName]
   aws_dimension_select:
     ClusterName: [hilti-main]
     ServiceName: [hilti-carina-data-management]
   aws_statistics: [Average]

CONF
}

resource "aws_iam_role" "cloudwatch-exporter" {
  count = local.create_resources

  name = "${local.name}-cloudwatch-exporter"

  assume_role_policy = data.aws_iam_policy_document.cloudwatch-exporter-assume[0].json

  permissions_boundary = var.iam_boundary
}

data "aws_iam_policy_document" "cloudwatch-exporter-assume" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = [
      "sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [
        module.ig-shared-resources.iam_nodes_role_arn]
    }
  }

  statement {
    actions = [
      "sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = [
        "ec2.${data.aws_partition.current.dns_suffix}"
      ]
    }
  }
}

data "aws_iam_policy_document" "cloudwatch-exporter" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = [
      "cloudwatch:*",
    ]

    effect = "Allow"

    resources = [
      "*"]
  }
}

resource "aws_iam_policy" "cloudwatch-exporter" {
  count = local.create_resources

  name = "${local.name}-cloudwatch-exporter"

  policy = data.aws_iam_policy_document.cloudwatch-exporter[0].json
}

resource "aws_iam_role_policy_attachment" "cloudwatch-exporter" {
  count      = local.create_resources
  role       = aws_iam_role.cloudwatch-exporter[0].name
  policy_arn = aws_iam_policy.cloudwatch-exporter[0].arn
}

resource "aws_route53_record" "addon-cloudwatch-exporter-internal" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = "cloudwatch-exporter-internal.${var.dns_suffix}"
  type     = "CNAME"

  ttl = 60

  # TODO: Temporary
  records = [
    lookup(local.nginx_ingress_loadbalancer, var.environment)]
}
