resource "tls_private_key" "acme_main_key" {
  count     = local.is_legacy_env ? 1 : 0
  algorithm = "RSA"
}

resource "acme_registration" "main" {
  count           = local.is_legacy_env ? 1 : 0
  account_key_pem = tls_private_key.acme_main_key[0].private_key_pem
  email_address   = "admin@${local.dns_arch_main_name}"
}

resource "tls_private_key" "acme_services_key" {
  count     = local.is_legacy_env ? 1 : 0
  algorithm = "RSA"
}

resource "acme_registration" "services" {
  count           = local.is_legacy_env ? 1 : 0
  account_key_pem = tls_private_key.acme_services_key[0].private_key_pem
  email_address   = "admin@${local.dns_arch_services_name}"
}

resource "acme_certificate" "main" {
  count           = local.is_legacy_env ? 1 : 0
  account_key_pem = acme_registration.main[0].account_key_pem
  common_name     = local.dns_arch_main_name
  subject_alternative_names = [
    "*.${local.dns_arch_main_suffix}",
  ]

  dns_challenge {
    provider = "route53"
    config = {
      AWS_DEFAULT_REGION    = var.aws_region
      AWS_ACCESS_KEY_ID     = local.arch_access_user
      AWS_SECRET_ACCESS_KEY = local.arch_access_key
    }
  }
}

resource "aws_iam_server_certificate" "legacy-cert-main" {
  count             = local.is_legacy_env ? 1 : 0
  name_prefix       = "${var.module_prefix}main-"
  certificate_body  = acme_certificate.main[0].certificate_pem
  certificate_chain = acme_certificate.main[0].issuer_pem
  private_key       = acme_certificate.main[0].private_key_pem

  lifecycle {
    create_before_destroy = true
  }
}

resource "acme_certificate" "services" {
  count           = local.is_legacy_env ? 1 : 0
  account_key_pem = acme_registration.services[0].account_key_pem
  common_name     = local.dns_arch_services_name
  subject_alternative_names = [
    "*.${local.dns_arch_services_suffix}",
  ]

  dns_challenge {
    provider = "route53"
    config = {
      AWS_DEFAULT_REGION    = var.aws_region
      AWS_ACCESS_KEY_ID     = local.arch_access_user
      AWS_SECRET_ACCESS_KEY = local.arch_access_key
    }
  }
}

resource "aws_iam_server_certificate" "legacy-cert-services" {
  count             = local.is_legacy_env ? 1 : 0
  name_prefix       = "${var.module_prefix}services-"
  certificate_body  = acme_certificate.services[0].certificate_pem
  certificate_chain = acme_certificate.services[0].issuer_pem
  private_key       = acme_certificate.services[0].private_key_pem

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "hilti-cloud" {
  count    = local.is_legacy_env ? 0 : 1
  provider = aws.route53
  name     = "${var.dns_managed_name}."
}

data "aws_acm_certificate" "hilti-cloud" {
  count       = local.is_legacy_env ? 0 : 1
  domain      = "*.${local.managed_env}.${var.dns_managed_name}"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}