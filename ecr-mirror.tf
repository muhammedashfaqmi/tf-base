resource "aws_ecr_repository" "mirror" {
  name = "${var.module_prefix}mirror"

  tags = local.tags
}
