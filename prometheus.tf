module "prometheus" {
  source = "./prometheus"

  providers = {
    aws         = aws
    aws.route53 = aws.route53
  }

  ami = data.aws_ami.aws_linux.image_id

  aws_region      = var.aws_region
  aws_zones       = [
    local.vpc_zones[0],
    local.vpc_zones[1],
    local.vpc_zones[2],
  ]
  aws_vpc_subnets = [
    local.vpc_subnets[0],
    local.vpc_subnets[1],
    local.vpc_subnets[2],
  ]
  aws_vpc_id      = local.vpc_id

  prometheus_count = "1"

  use_public_network      = local.use_public_network
  module_prefix           = var.module_prefix
  key_name                = aws_key_pair.user.key_name
  infra_bucket            = aws_s3_bucket.infra.bucket
  infra_prefix            = var.infra_prefix
  prometheus_cache_bucket = aws_s3_bucket.infra.bucket
  iam_boundary            = local.permission_boundary
  environment             = var.environment

  tags = merge(local.tags, {
    format(local.shared_tag, "role") = "Prometheus"
  })

  prometheus_tags           = [
    "aws",
    "type-docker",
    format("%s-%s", "environment", lower(var.environment)),
  ]
  prometheus_instance_type  = local.prometheus_config.linux.type
  prometheus_version        = "v2.10.0"
  grafana_version           = "6.2.5"
  prometheus_limit          = "20"
  prometheus_root_disk_size = "20"
  prometheus_disk_size      = "500"
  prometheus_disk_inodes    = "67108864"
  prometheus_cache_prefix   = "prometheus/cache/"
  prometheus_type           = "docker"

  dns_create = true
  dns_zone   = local.dns_main_zone
  dns_suffix = local.dns_main_suffix
}
