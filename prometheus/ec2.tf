resource "aws_security_group" "prometheus" {
  count       = local.create-instances
  name        = local.name
  description = "Security group for Prometheus instance"
  vpc_id      = var.aws_vpc_id

  tags = local.tags
}

resource "aws_security_group_rule" "prometheus-egress" {
  count             = local.create-instances
  type              = "egress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "prometheus-ingress-ssh" {
  count             = var.prometheus_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "prometheus-ingress-prometheus" {
  count             = var.prometheus_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 9090
  to_port           = 9090
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "prometheus-ingress-cw-exporter" {
  count             = var.prometheus_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 9106
  to_port           = 9106
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "prometheus-ingress-cadvisor" {
  count             = var.prometheus_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 9091
  to_port           = 9091
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "prometheus-ingress-node-exporter" {
  count             = var.prometheus_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 9100
  to_port           = 9100
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "prometheus-ingress-grafana" {
  count             = var.prometheus_type == "docker" ? local.create-instances : 0
  type              = "ingress"
  security_group_id = aws_security_group.prometheus[0].id
  from_port         = 80 
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "prometheus" {
  count             = var.prometheus_count
  ami               = var.ami
  instance_type     = var.prometheus_instance_type
  availability_zone = var.aws_zones[count.index]

  vpc_security_group_ids = [aws_security_group.prometheus[0].id]
  iam_instance_profile   = aws_iam_instance_profile.prometheus[0].id

  key_name = var.key_name

  subnet_id = var.aws_vpc_subnets[count.index]

  user_data = data.null_data_source.prometheus-data[count.index].outputs[format("init-%s", var.prometheus_type)]

  root_block_device {
    volume_type = "gp2"
    volume_size = var.prometheus_root_disk_size
  }

  ebs_block_device {
    device_name           = "/dev/sdh"
    volume_type           = "gp2"
    volume_size           = var.prometheus_disk_size
    encrypted             = var.prometheus_type == "docker"
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [user_data]
  }

  depends_on = [
    aws_cloudwatch_log_group.prometheus,
    aws_iam_role_policy.prometheus,
  ]

  tags = merge(
    local.tags,
    {
      "Name" = format("%s-%d", local.local-tags["Name"], count.index + 1)
    }
  )
  volume_tags = merge(
    local.tags,
    {
      "Name" = format("%s-%d", local.local-tags["Name"], count.index + 1)
    }
  )
}

locals {
  create-instances = var.prometheus_count > 0 ? 1 : 0
}

