data "template_file" "oag-base" {
  template = file("${path.module}/templates/oag-base.ldif")

  vars = {
    ENVIRONMENT = lower(var.environment)
  }
}

resource "aws_s3_bucket_object" "oag-base" {
  count = var.create ? 1 : 0

  bucket  = var.infra_bucket
  key     = "${local.oag_prefix}base.ldif"
  content = data.template_file.oag-base.rendered
  etag    = md5(data.template_file.oag-base.rendered)

  tags = var.tags

  content_type = "text/plain"
}

locals {
  oag_prefix = "${var.infra_prefix}oag-templates/"
}

