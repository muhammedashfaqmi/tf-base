data "aws_caller_identity" "current" {
}

data "aws_partition" "current" {}

data "aws_iam_policy" "permission-boundary" {
  arn = "arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:policy/${local.env.iam_permission_boundary}"
}

# https://aws.amazon.com/amazon-linux-ami/
data "aws_ami" "aws_linux" {
  owners      = ["951794384439"]

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "image-id"
    values = ["ami-09ae0d187c5ad7bef"]
  }

  filter {
    name   = "name"
    values = ["Hilti-AmazonLinux_110620"]
  }
}

data "aws_ami" "ami_windows_2019" {
  filter {
    name   = "name"
    values = [
      "Windows_Server-2019-English-Core-EKS_Optimized-1.14-2020.06.10"
    ]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  #owners = [local.env.is_legacy ? "801119661308" : "873417489675"]
  owners = [local.env.is_legacy ? "801119661308" : "amazon"]
}

//data "aws_ami" "aws_windows_2016" {
//  filter {
//    name   = "name"
//    values = [
//      "HILTI-WinSvr2016Base-AMI-0950-2019-10-17",
//#      "Windows_Server-2016-English-Full-Containers-2019.04.21",
//    ]
//  }
//
//  filter {
//    name   = "virtualization-type"
//    values = ["hvm"]
//  }
//
//  #owners = [local.env.is_legacy ? "801119661308" : "873417489675"]
//  owners = [local.env.is_legacy ? "801119661308" : "288340643444"]
//}
//
//data "aws_ami" "aws_windows_2012" {
//  filter {
//    name   = "name"
//    values = [
//      "HILTI-WinSvr2012R2Base-AMI-0444-2019-10-21",
//#      "Windows_Server-2012-R2_RTM-English-64Bit-Base-2019.04.26",
//    ]
//  }
//
//  filter {
//    name   = "virtualization-type"
//    values = ["hvm"]
//  }
//
//  #owners = [local.env.is_legacy ? "801119661308" : "873417489675"]
//  owners = [local.env.is_legacy ? "801119661308" : "288340643444"]
//}
