module "ig-shared-resources" {
  source = "./ig-shared-resources"

  create           = var.create
  tags             = local.tags
  name             = local.name
  vpc_id           = var.vpc_id
  sg_master_id     = var.create ? aws_security_group.k8s-master[0].id : ""
  sg_sources_count = var.api_sg_count + 1
  sg_sources_ids   = local.api_sources_sgs_ids
  iam_boundary     = var.iam_boundary
}
