module "ansible" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/ansible/0.1.0-master.zip"
  create = false

  project     = null
  service     = "infra"
  name        = local.name
  environment = var.environment
  tags        = local.tags

  use_remote_state = false

  module_prefix = var.module_prefix
  shared_tag    = var.shared_tag

  ansible_dir_source     = local.data_dir
  ansible_dir_target     = local.ansible_directory
  ansible_tags           = local.ansible_tags
  ansible_variables_json = local.ansible_variables_json
}

locals {
  ansible_variables_json = var.create ? jsonencode({
    name_full = local.name

    istio_manifest = local.manifest

    helm = {
      namespace  = var.k8s_namespace
      deployment = jsonencode({
        spec = {
          template = {
            spec = {
              namespace    = var.k8s_namespace
              tolerations  = local.k8s_default_config.tolerations
              nodeSelector = local.k8s_default_config.nodeSelector
            }
          }
        }
      })
    }

    kiali = local.ansible_kiali
  }) : "{}"
}
