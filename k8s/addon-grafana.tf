locals {
  addon_grafana = {
    chart_version = "3.12.1"
    name          = "grafana"
    namespace     = local.system_srv_namespace
    helm_values   = {

      tolerations  = local.system_srv_tolerations
      nodeSelector = local.system_srv_node_selector

      "grafana.ini" = {
        "auth.anonymous" = {
          enabled  = true
          org_role = "Admin"
        }

        paths       = {
          data         = "/var/lib/grafana/data"
          logs         = "/var/log/grafana"
          plugins      = "/var/lib/grafana/plugins"
          provisioning = "/etc/grafana/provisioning"
        }
        analytics   = {
          check_for_updates = true
        }
        log         = {
          mode = "console"
        }
        grafana_net = {
          url = "https://grafana.net"
        }
      }

      sidecar = {
        datasources = {
          enabled = "true"
          label   = "asws.hilti.com/grafana-datasource-prometheus-server"
        }
        dashboards  = {
          enabled = "true"
          label   = "asws.hilti.com/grafana-dashboard"
        }
      }

      ingress = {
        enabled = true

        annotations = {
          "nginx.ingress.kubernetes.io/backend-protocol" = "HTTP"

          //"kubernetes.io/tls-acme" = "true"
          //"certmanager.k8s.io/cluster-issuer" = "self-signed"
        }

        hosts = !var.create ? [] : [
          aws_route53_record.addon-grafana-local[0].name,
        ]

        # Temporary debugging
        //        tls = [{
        //          secretName = "grafana-tls"
        //
        //          hosts = [
        //            aws_route53_record.addon-grafana-local[0].name,
        //          ]
        //        }]
      }

      resources = {
        requests = {
          cpu    = "100m"
          memory = "300Mi"
        }
        limits   = {
          cpu    = "100m"
          memory = "300Mi"
        }
      }
    }
  }
}

resource "aws_route53_record" "addon-grafana-local" {
  count = local.create_resources

  provider = aws.route53
  zone_id  = var.dns_zone
  name     = "grafana-internal.${var.dns_suffix}"
  type     = "CNAME"

  ttl = 60

  records = [
    aws_lb.ingress[0].dns_name,
  ]
}
