module "istio" {
  source = "./istio"

  create = var.create

  istio_version = "1.4.2"

  env                     = var.env
  tags                    = var.tags
  name                    = "${var.name}-istio"
  environment             = var.environment
  module_prefix           = var.module_prefix
  shared_tag              = var.shared_tag
  aws_region              = var.aws_region
  iam_boundary            = var.iam_boundary
  infra_bucket            = var.infra_bucket
  k8s_cluster             = local.k8s_name
  vpc_id                  = var.vpc_id
  vpc_subnets             = var.vpc_subnets
  ec2_linux_ami           = var.create?data.aws_ami.eks_linux[0].id:null
  k8s_ec2_nodes_sg_id     = module.ig-shared-resources.ec2_nodes_sg_id
  k8s_iam_nodes_role_name = module.ig-shared-resources.iam_nodes_role_name
  k8s_full_tolerations    = local.system_srv_tolerations_all
}
