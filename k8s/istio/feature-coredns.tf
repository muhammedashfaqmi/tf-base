locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CoreDNSFeatureSpec
  feature_coredns = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#CoreDNSComponentSpec
      coreDNS = {
        enabled = true

        k8s = merge(local.k8s_default_config, {})
      }
    }
  }

  # https://istio.io/docs/reference/config/installation-options/#istiocoredns-options
  values_coredns = {
    enabled = true

    coreDNSImage       = "coredns/coredns"
    coreDNSPluginImage = "istio/coredns-plugin:0.2-istio-1.1"
  }
}
