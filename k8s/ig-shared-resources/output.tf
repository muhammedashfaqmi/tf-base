output "iam_nodes_role_name" {
  value = var.create ? local.iam_role_name : null
}

output "iam_nodes_role_arn" {
  value = var.create ? aws_iam_role.ec2-nodes[0].arn : null
}

output "iam_default_pod_role_arn" {
  value = var.create ? aws_iam_role.default-pod-role[0].arn : null

  depends_on = [
    aws_iam_role.default-pod-role,
    aws_iam_role_policy_attachment.default-pod-role,
  ]
}

output "ec2_nodes_sg_id" {
  value = var.create ?aws_security_group.ec2-nodes[0].id: null

  depends_on = [
    aws_security_group.ec2-nodes,
  ]
}
