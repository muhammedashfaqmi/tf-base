output "templates_prefix" {
  value = local.oag_prefix
}

output "templates_bucket" {
  value = var.infra_bucket
}

