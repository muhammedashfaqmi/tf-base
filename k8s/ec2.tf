resource "aws_lb" "ingress" {
  count = local.create_resources

  name = "${local.name}-ingress"

  subnets = var.vpc_subnets

  internal           = !var.use_public_network
  load_balancer_type = "network"

  tags = local.tags
}


resource "aws_lb_listener" "ingress" {
  count = local.create_resources * length(local.ingress_ports)

  load_balancer_arn = aws_lb.ingress[0].id
  port              = local.ingress_ports[count.index]
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.ingress[count.index].id
    type             = "forward"
  }
}

# TODO: https://aws.amazon.com/blogs/networking-and-content-delivery/using-static-ip-addresses-for-application-load-balancers/
resource "aws_lb_target_group" "ingress" {
  count = local.create_resources * length(local.ingress_ports)

  name_prefix = substr(local.name, 0, length(local.name) > 6 ? 6 : length(local.name))
  port        = local.ingress_ports[count.index]
  protocol    = "TCP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  lifecycle {
    create_before_destroy = true
  }

  health_check {
    enabled             = true
    protocol            = "TCP"
    port                = local.ingress_ports[count.index]
    interval            = 10
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = local.tags
}
