locals {
  # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#TelemetryFeatureSpec
  feature_telemetry = {
    enabled = true

    components = {
      # https://istio.io/docs/reference/config/istio.operator.v1alpha12.pb/#TelemetryComponentSpec
      telemetry = {
        enabled = true

        k8s = merge(local.k8s_default_config, {
          env = [
            {
              name      = "POD_NAMESPACE"
              valueFrom = {
                fieldRef = {
                  apiVersion = "v1"
                  fieldPath  = "metadata.namespace"
                }
              }
            },
            {
              name  = "GOMAXPROCS"
              value = "6"
            }
          ]

          hpaSpec = {
            minReplicas    = 2
            maxReplicas    = 5
            metrics        = [
              {
                resource = {
                  name                     = "cpu"
                  targetAverageUtilization = 80
                }
                type     = "Resource"
              }
            ]
            scaleTargetRef = {
              apiVersion = "apps/v1"
              kind       = "Deployment"
              name       = "istio-telemetry"
            }
          }

          resources = {
            requests = {
              cpu    = "50m"
              memory = "100Mi"
            }
            limits   = {
              cpu    = "4800m"
              memory = "4G"
            }
          }
        })
      }
    }
  }

  ansible_kiali = {
    namespace = var.k8s_namespace
    username  = random_string.kiali_username.result
    password  = random_string.kiali_password.result
  }
}

resource "random_string" "kiali_username" {
  length  = 16
  special = false
  upper   = true
  lower   = true
}

resource "random_string" "kiali_password" {
  length  = 16
  special = false
  upper   = true
  lower   = true
}
