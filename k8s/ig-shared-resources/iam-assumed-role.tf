data "aws_iam_policy_document" "ec2-nodes-linux-assume" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"

    resources = ["*"]
  }
}

resource "aws_iam_policy" "ec2-nodes-linux-assume" {
  count = local.create_resources

  name = "${aws_iam_role.ec2-nodes[0].name}-assume"

  policy = data.aws_iam_policy_document.ec2-nodes-linux-assume[0].json
}

resource "aws_iam_role_policy_attachment" "ec2-nodes-linux-assume" {
  count      = local.create_resources
  role       = aws_iam_role.ec2-nodes[0].name
  policy_arn = aws_iam_policy.ec2-nodes-linux-assume[0].arn
}

data "aws_iam_policy_document" "default-pod-role-assume" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [aws_iam_role.ec2-nodes[0].arn]
    }
  }

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.${data.aws_partition.current.dns_suffix}"]
    }
  }
}

resource "aws_iam_role" "default-pod-role" {
  count = local.create_resources

  name = "${local.name}-default-pod-role"

  assume_role_policy = data.aws_iam_policy_document.default-pod-role-assume[0].json

  permissions_boundary = var.iam_boundary
}

data "aws_iam_policy_document" "default-pod-role" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    actions = [
      "ec2:*"
    ]

    effect = "Allow"

    resources = ["*"]
  }
}

resource "aws_iam_policy" "default-pod-role" {
  count = local.create_resources

  name = "${local.name}-default-pod-role"

  policy = data.aws_iam_policy_document.default-pod-role[0].json
}

resource "aws_iam_role_policy_attachment" "default-pod-role" {
  count      = local.create_resources
  role       = aws_iam_role.default-pod-role[0].name
  policy_arn = aws_iam_policy.default-pod-role[0].arn
}
